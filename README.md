# Frood Dashboard Python App Docker

# Fetch branch environment to test the functionality

## Prerequisites
* Please install python3 and pip3 on your machine.
* pip3 install -r requirements.txt

## To run on server

* Setup uwsgi server.
* place froodapp.ini file in /etc/uwsgi/sites/ folder
* create a file /etc/init/uwsgi.conf
* Start uwsgi server using sudo service uwsgi start/stop

# To test the app we need memcache installed and its port and host needs to be mentioned in config_sample.py (equivalent).