FROM python:3

COPY requirements.txt .
RUN pip install uwsgi && pip install -r requirements.txt

RUN mkdir -p /app/dashboard && mkdir -p /app/config && mkdir -p /var/log/uwsgi

EXPOSE 8080

COPY frood-proxy /app/dashboard/ 

ENTRYPOINT ["uwsgi", "--ini", "/app/dashboard/froodapp.ini"]
